const initialState = {
    players: {
        1: { name: "John", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 1 },
        2: { name: "Jack", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 2 },
        3: { name: "Jessy", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 3 },
        4: { name: "Jenny", pv: 100, pvMax: 100, mana: 30, manaMax: 30, id: 4 }
      },
    monster: {pv:800, pvMax:800}
  };
   
  function rootReducer(state = initialState, action) {
    if(action.type === "HIT_MONSTER"){
      return {...state,
              monster: {...state.monster, 
                pv : state.monster.pv -50
              }
            }
    }
    if(action.type === "HIT_BACK"){
      const id = action.payload
      return  {...state,
              players: { ...state.players,
                        [id] : { ...state.players[id], 
                              pv: state.players[id].pv -5 }
                      }
      }
    }
    return state;
  };
   
  export default rootReducer;