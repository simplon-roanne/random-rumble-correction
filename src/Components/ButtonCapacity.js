import React from 'react';
import {useDispatch} from 'react-redux';

const ButtonCapacity = (props) => {

    const dispatch = useDispatch()
    const hitMonster = () => dispatch({type: "HIT_MONSTER"})
    const hitback = () => dispatch({type: "HIT_BACK", payload : props.player.id})

    const combat = () => {
        hitMonster()
        hitback()
    }
    
    return (
        <button type="button" onClick={() => combat()} className="btn btn-success material-tooltip-main ">
            hit
            <i className="fas fa-bomb"></i> 5
            <i className="fas fa-fire-alt"></i> - 5
        </button>
    )

}



export default ButtonCapacity;